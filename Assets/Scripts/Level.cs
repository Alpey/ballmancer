﻿using System;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] private Transform shapesContainer;
    [SerializeField] private LevelData levelData;
    public int ShapesAmount { get; private set; }
    public int BallsAvailable { get; private set; }
    
    public static event Action<int> OnLevelInit;
    public static event Action<int, int> OnScoreUpdate;
    
    private int _currentBalls;
    private int _currentShapes;

    private void Start()
    {
        _currentShapes = ShapesAmount = shapesContainer.childCount;
        _currentBalls = BallsAvailable = levelData.BallsAmount;
        OnLevelInit?.Invoke(BallsAvailable);
    }

    private void OnEnable()
    {
        FallTrigger.OnShapeFell += OnShapeFell;
        BallShooter.OnBallShot += OnBallShot;
    }

    private void OnDisable()
    {
        FallTrigger.OnShapeFell -= OnShapeFell;
        BallShooter.OnBallShot -= OnBallShot;
    }
    
    private void OnBallShot()
    {
        if (--_currentBalls <= 0)
        {
            _currentBalls = 0;
            if (_currentShapes > 0)
            {
                GameManager.Instance.LevelEnd(false);
            }
        }
        OnScoreUpdate?.Invoke(_currentBalls, _currentShapes);
    }

    private void OnShapeFell()
    {
        if (--_currentShapes <= 0)
        {
            _currentShapes = 0;
            if (_currentBalls > 0)
            {
                GameManager.Instance.LevelEnd(true);
            }
        }
        OnScoreUpdate?.Invoke(_currentBalls, _currentShapes);
    }
}
