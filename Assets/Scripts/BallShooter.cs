﻿using System;
using UnityEngine;

public class BallShooter : MonoBehaviour
{
    [SerializeField] private Transform shootingPoint;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private float shootingForce;

    public static event Action OnBallShot;

    private void Update()
    {
        InputProcessing();
    }

    private void InputProcessing()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShootBall();
            OnBallShot?.Invoke();
        }
    }

    private void ShootBall()
    {
        Vector3 shootPos = shootingPoint.position;
        RaycastHit hit; 
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            Vector3 shootDirection = hit.point - shootPos;
            GameObject ball = Instantiate(ballPrefab, shootPos, Quaternion.LookRotation(shootDirection));
            ball.GetComponent<Rigidbody>().AddForce(shootingForce * 200f * ball.transform.forward);
        }
    }
}
