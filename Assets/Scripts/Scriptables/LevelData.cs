﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/LevelData", fileName = "LevelData_01")]
public class LevelData : ScriptableObject
{
    [SerializeField] private int ballsAmount;

    public int BallsAmount => ballsAmount;
}
