﻿using System;
using UnityEngine;

public class FallTrigger : MonoBehaviour
{
    [SerializeField] private string shapeTag = "Shape";
    [SerializeField] private string ballTag = "Ball";
    
    public static event Action OnShapeFell;
    public static event Action OnBallFell;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(shapeTag))
        {
            OnShapeFell?.Invoke();
        }
        if (other.CompareTag(ballTag))
        {
            OnBallFell?.Invoke();
        }
    }
}
