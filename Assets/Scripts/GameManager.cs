﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Singleton

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    #endregion

    [SerializeField] private Animator fader;
    
    public event Action OnWin;
    public event Action OnLose;
    public event Action OnNewLevelLoading;

    private int _currentLevel = 1;

    public int CurrentLevel => _currentLevel;
    public int LevelsAmount => SceneManager.sceneCountInBuildSettings - 1;

    private void Start()
    {
        LoadFirstLevel();
    }

    public void LevelEnd(bool win)
    {
        if (win)
        {
            Debug.Log("Win");
            OnWin?.Invoke();
            
            StartCoroutine(LoadLevel(GetNextLevelIndex()));
        }
        else
        {
            Debug.Log("Lose");
            OnLose?.Invoke();
            
            StartCoroutine(LoadLevel(_currentLevel));
        }
    }

    private void LoadFirstLevel()
    {
        _currentLevel = 1;
        SceneManager.LoadScene(_currentLevel, LoadSceneMode.Additive);
    }

    private IEnumerator LoadLevel(int levelToLoad)
    {
        yield return new WaitForSeconds(.75f);

        fader.SetTrigger("FadeOut");

        yield return new WaitForSeconds(.75f);

        AsyncOperation unload = SceneManager.UnloadSceneAsync(_currentLevel);

        while (!unload.isDone)
        {
            yield return 0;
        }

        _currentLevel = levelToLoad;
        SceneManager.LoadScene(_currentLevel, LoadSceneMode.Additive);

        yield return 0;
        
        fader.SetTrigger("FadeIn");
        
        OnNewLevelLoading?.Invoke();
    }

    private int GetNextLevelIndex()
    {
        return Math.Min(_currentLevel + 1, SceneManager.sceneCountInBuildSettings);
    }
}
