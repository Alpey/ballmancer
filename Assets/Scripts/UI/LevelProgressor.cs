﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgressor : MonoBehaviour
{
    [SerializeField] private LevelProgressBlob levelBlob;
    
    private List<LevelProgressBlob> _levelBlobs = new List<LevelProgressBlob>();

    private void Start()
    {
        GameManager.Instance.OnNewLevelLoading += UpdateProgression;
        Init();
        UpdateProgression();
    }

    private void OnDisable()
    {
        GameManager.Instance.OnNewLevelLoading -= UpdateProgression;
    }

    private void Init()
    {
        var levelsNum = GameManager.Instance.LevelsAmount;

        for (int i = 0; i < levelsNum; i++)
        {
            LevelProgressBlob blob = Instantiate(levelBlob, levelBlob.transform.parent);
            _levelBlobs.Add(blob);
        }
        
        levelBlob.gameObject.SetActive(false);
    }

    private void UpdateProgression()
    {
        var currentLevelIndex = GameManager.Instance.CurrentLevel - 1;

        for (int i = 0; i < _levelBlobs.Count; i++)
        {
            if (i < currentLevelIndex)
            {
                _levelBlobs[i].SetComplete();
            }
            else if (i == currentLevelIndex)
            {
                _levelBlobs[i].SetActive(true);
            }
        }
    }
}
