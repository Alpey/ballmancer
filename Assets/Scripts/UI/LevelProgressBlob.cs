﻿using UnityEngine;
using UnityEngine.UI;

public class LevelProgressBlob : MonoBehaviour
{
    [SerializeField] private GameObject activeObj;
    [SerializeField] private Image inactiveImage;
    [SerializeField] private Color incompleteColor;
    [SerializeField] private Color completeColor;

    public void SetActive(bool active)
    {
        activeObj.SetActive(active);
    }

    public void SetComplete()
    {
        SetActive(false);
        inactiveImage.color = completeColor;
    }
    
    public void SetIncomplete()
    {
        inactiveImage.color = incompleteColor;
    }
}
