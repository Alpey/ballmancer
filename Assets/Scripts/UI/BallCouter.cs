﻿using System;
using TMPro;
using UnityEngine;

public class BallCouter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI ballConterText;

    private void OnEnable()
    {
        Level.OnLevelInit += OnLevelInit;
        Level.OnScoreUpdate += OnScoreUpdate;
    }

    private void OnDisable()
    {
        Level.OnLevelInit -= OnLevelInit;
        Level.OnScoreUpdate -= OnScoreUpdate;
    }

    private void OnLevelInit(int balls)
    {
        UpdateText(balls.ToString());
    }

    private void OnScoreUpdate(int balls, int shapes)
    {
        UpdateText(balls.ToString());
    }

    private void UpdateText(string text)
    {
        ballConterText.text = text;
    }
}
